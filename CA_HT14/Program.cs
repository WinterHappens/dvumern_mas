﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//11111
//01110
//00100
//01110
//11111

namespace CA_HT14
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 5;
            int m = 5;
            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = 1;
                }
            }

            for (int j = 0; j < m / 2; j++)
            {
                for (int i = j + 1; i < n - j - 1; i++)
                {
                    mas[i, j] = 0;
                    mas[i, m - j - 1] = 0;
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}

