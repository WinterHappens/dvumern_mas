﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//10001
//11011
//11111
//11011
//10001

namespace CA_HT13
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 5;
            int m = 5;
            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = 1;
                }
            }

            for (int i = 0; i < n/2; i++)
            {
                for (int j = i + 1; j < m - i - 1; j++)
                {
                    mas[i, j] = 0;
                    mas[n - i - 1, j] = 0;
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
