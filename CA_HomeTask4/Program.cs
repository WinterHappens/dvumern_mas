﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//В поезде 18 вагонов, в каждом из которых 36 мест. Информация о проданных
//на поезд билетах хранится в двумерном массиве, номера строк которых
//соответствуют номерам вагонов, а номера столбцов - номерам мест.
//Если билет на то или иное место продан, то соответствующий элемент
//массива имеет значение 1, в противном случае - 0. 
//Составить программу, определяющую число свободных мест в любом из вагонов поезда.

namespace CA_HomeTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            int wagons = 18;
            int places = 36;
            int k = 0;

            int[,] mas = new int[wagons, places];

            Random rnd = new Random();

            for (int i = 0; i < wagons; i++)
            {
                for (int j = 0; j < places; j++)
                {
                    mas[i, j] = rnd.Next(0, 2);
                }
            }

            for (int i = 0; i < wagons; i++)
            {
                for (int j = 0; j < places; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }

            int numberOfWagon = 0;
            do
            {
                Console.Write("Введите номер Вагона: ");
                numberOfWagon = int.Parse(Console.ReadLine()) - 1;
            } while (numberOfWagon < 1 || numberOfWagon > 18);

            for (int j = 0; j < places; j++)
            {                
                if (mas[numberOfWagon,j] == 0)
                {
                    k++;
                }
            }

            Console.WriteLine($"Количество свободных мест в {numberOfWagon + 1} вагоне равно {k}");

            Console.ReadKey();
        }
    }
}
