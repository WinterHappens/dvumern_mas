﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//сделать реверс всех строк

namespace CA_HomeTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 4;
            int m = 4;
            int temp;

            int[,] mas = new int[n, m];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(0, 100);
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + "  ");
                }
                Console.WriteLine();
            }


            for (int j = 0; j < m; j++)
            {
                int k = n - 1;
                for (int i = 0; i < n / 2; i++)
                {
                    temp = mas[i, j];
                    mas[i, j] = mas[k, j];
                    mas[k, j] = temp;
                    k--;
                }
                Console.WriteLine();
            }


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + "  ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
