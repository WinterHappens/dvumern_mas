﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//123456
//234561
//345612
//456123
//561234
//612345

namespace CA_HT15
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 6;
            int m = 6;
            int[,] mas = new int[n, m];

            int start = 1;
            int tmp = 0;

            for (int j = 0; j < m; j++)
            {
                mas[0, j] = start;
                start++;
            }

            for (int i = 1; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = mas[i - 1, j];
                }
                tmp = mas[i, 0];
                for (int j = 0; j < m - 1; j++)
                {
                    mas[i, j] = mas[i, j + 1];
                }
                mas[i, m - 1] = tmp;
            }


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
