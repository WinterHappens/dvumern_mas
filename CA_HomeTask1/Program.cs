﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//сделать сравнение 2х длинных чисел
//выдать 1 если 1 > 2
//выдать -1 если 1 < 2
//выдать 0 если 1 == 2

namespace CA_HomeTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int n = 2;
            int m = 10;
            bool same = false;

            int[,] mas = new int[n,m];


             for (int i = 0; i < n; i++)
             {
                 for (int j = 0; j < m; j++)
                 {
                     mas[i, j] = rnd.Next(0, 10);
                 }
             }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j]);
                }
                Console.WriteLine();
            }

            for (int j = 0; j < m; j++)
            {
                if(mas[0,j] > mas[1,j])
                {
                    Console.Write("1");
                    same = false;
                    j = m;
                }
                else if (mas[0, j] < mas[1, j])
                {
                    Console.Write("-1");
                    same = false;
                    j = m;
                }
                else
                {
                    same = true;
                }
            }

            if(same == true)
            {
                Console.Write("0");
            }

            Console.ReadKey();
        }
    }
}
