﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 3;
            int m = 4;
            int findI = -1;
            int findJ = -1;;
            int[,] mas = new int[n, m];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(0, 100);
                }
            }

            
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (mas[i, j] % 2 == 0)
                    {
                        findI = i;
                        findJ = j;
                        i = m;
                        j = n;
                    }
                }
            }

            Console.Write(findI + "-" + findJ);
            Console.ReadKey();

        }
    }
}
