﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 1  2  3  4  5 
//16 17 18 19  6
//15 24 25 20  7
//14 23 22 21  8
//13 12 11 10  9

namespace CA_HT16
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = 5;
            int m = 5;
            int[,] mas = new int[n, m];

            int start = 1;
            int leftBorderI = 0;
            int leftBorderJ = 0;
            int rightBorderJ = m;
            int rightBorderI = n;

            for (int i = leftBorderI; i < rightBorderI; i++)
            {
                for (int j = leftBorderJ; j < rightBorderJ; j++)
                {
                    mas[leftBorderJ, j] = start;
                    start++;
                }
                rightBorderJ--;

                for (int k = leftBorderI + 1; k < rightBorderI; k++)
                {
                    mas[k, rightBorderJ] = start;
                    start++;
                }
                rightBorderI--;

                for (int l = rightBorderJ - 1; l >= leftBorderJ; l--)
                {
                    mas[rightBorderI, l] = start;
                    start++;
                }
                leftBorderJ++;

                for (int p = rightBorderI - 1; p > leftBorderI; p--)
                {
                    mas[p, leftBorderJ - 1] = start;
                    start++;
                }
                leftBorderI++;
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + "  ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
