﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//10000
//11000
//11100
//11110
//11111

namespace CA_HT01
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 5;
            int m = 5;
            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = 0;
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    mas[i, j] = 1;
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j]+" ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
