﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 3;
            int m = 4;
            int maxI = -1;
            int sumI = 0;
            int sumMax = sumI;

            int[,] mas = new int[n, m];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(0, 100);
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            for (int i = 0; i < n; i++)
            {
                sumI = 0;
                for (int j = 0; j < m; j++)
                {
                    sumI += mas[i, j];                    
                }

                if (sumI > sumMax)
                {
                    maxI = i;
                    sumMax = sumI;
                }               
            }

            Console.Write(maxI);
            Console.ReadKey();

        }
    }
}
